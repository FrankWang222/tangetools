#!/bin/bash

test_tmp=`tempfile`
export test_tmp
export LC_ALL=C

opt_tester() {
    opt="$@"
    tmp=$(tempfile)
    test_2search() {
	xargs echo Search in < $tmp
	2search $opt $tmp 0 2 2.1 100000
	2search $opt -B $tmp 0 2 2.1 100000
    }
    (true) |
	sort $opt > $tmp
    echo Search in null file
    test_2search

    (echo) |
	sort $opt > $tmp
    echo Search in newline
    test_2search
    
    (echo 1.000000000) |
	sort $opt > $tmp
    test_2search

    (echo 1.000000000;
     echo 2) |
	sort $opt > $tmp
    test_2search

    (echo 1;
     echo 2.000000000) |
	sort $opt > $tmp
    test_2search

    (echo 1.000000000;
     echo 2;
     echo 3) |
	sort $opt > $tmp
    test_2search

    (echo 1;
     echo 2.000000000;
     echo 3) |
	sort $opt > $tmp
    test_2search
    
    (echo 1;
     echo 2;
     echo 3.000000000) |
	sort $opt > $tmp
    test_2search

    rm $tmp
}
export -f opt_tester

test_n() {
    tmp=${test_tmp}_n
    true > $tmp
    echo Search in null file
    2search -n $tmp 0 2 2.1 100000
    2search -nB $tmp 0 2 2.1 100000
    echo > $tmp
    xargs echo Search in newline
    2search -n $tmp 0 2 2.1 100000
    2search -nB $tmp 0 2 2.1 100000
    echo 1.000000000 > $tmp
    xargs echo Search in < $tmp
    2search -n $tmp 0 2 2.1 100000
    2search -nB $tmp 0 2 2.1 100000
    echo 1.000000000 > $tmp
    echo 2 >> $tmp
    xargs echo Search in < $tmp
    2search -n $tmp 0 2 2.1 100000
    2search -nB $tmp 0 2 2.1 100000
    echo 1 > $tmp
    echo 2.000000000 >> $tmp
    xargs echo Search in < $tmp
    2search -n $tmp 0 2 2.1 100000
    2search -nB $tmp 0 2 2.1 100000
    echo 1.000000000 > $tmp
    echo 2 >> $tmp
    echo 3 >> $tmp
    xargs echo Search in < $tmp
    2search -n $tmp 0 2 2.1 100000
    2search -nB $tmp 0 2 2.1 100000
    echo 1 > $tmp
    echo 2.000000000 >> $tmp
    echo 3 >> $tmp
    xargs echo Search in < $tmp
    2search -n $tmp 0 2 2.1 100000
    2search -nB $tmp 0 2 2.1 100000
    echo 1 > $tmp
    echo 2 >> $tmp
    echo 3.000000000 >> $tmp
    xargs echo Search in < $tmp
    2search -n $tmp 0 2 2.1 100000
    2search -nB $tmp 0 2 2.1 100000
    rm $tmp
}

test_n_opt() {
    opt_tester -n
}

test_rn_opt() {
    opt_tester -rn
}

test_r_opt() {
    opt_tester -r
}

test_k3N_2N_1n() {
    tmp=$(tempfile)
    cat >$tmp <<EOF
1	chr1	Sample 1
11	chr1	Sample 1
111	chr1	Sample 1
1111	chr1	Sample 1
11111	chr1	Sample 1
111111	chr1	Sample 1
1	chr2	Sample 1
22	chr2	Sample 1
111	chr2	Sample 1
2222	chr2	Sample 1
11111	chr2	Sample 1
111111	chr2	Sample 1
1	chr10	Sample 1
11	chr10	Sample 1
111	chr10	Sample 1
1111	chr10	Sample 1
11111	chr10	Sample 1
111111	chr10	Sample 1
1	chr1	Sample 2
11	chr1	Sample 2
111	chr1	Sample 2
1111	chr1	Sample 2
11111	chr1	Sample 2
111111	chr1	Sample 2
1	chr2	Sample 2
22	chr2	Sample 2
111	chr2	Sample 2
2222	chr2	Sample 2
11111	chr2	Sample 2
111111	chr2	Sample 2
1	chr10	Sample 2
11	chr10	Sample 2
111	chr10	Sample 2
1111	chr10	Sample 2
11111	chr10	Sample 2
111111	chr10	Sample 2
1	chr1	Sample 10
11	chr1	Sample 10
111	chr1	Sample 10
1111	chr1	Sample 10
11111	chr1	Sample 10
111111	chr1	Sample 10
1	chr2	Sample 10
22	chr2	Sample 10
111	chr2	Sample 10
2222	chr2	Sample 10
11111	chr2	Sample 10
111111	chr2	Sample 10
1	chr10	Sample 10
11	chr10	Sample 10
111	chr10	Sample 10
1111	chr10	Sample 10
11111	chr10	Sample 10
111111	chr10	Sample 10
EOF
    # Find the line with 111,chr2,Sample 10
    2grep -t '\t' -k3N,2N,1n $tmp 'Sample 10' chr2 111
}

test_partial_line() {
    tmp=$(tempfile)
    seq 100 | LC_ALL=C sort > $tmp
    echo '### 2search --grep'
    2search --grep $tmp 3
    echo '### 2grep'
    2grep $tmp 3
    echo '### ... | 2grep'
    echo 3 | 2grep $tmp
    rm $tmp
}

test_recstart() {
    tmp=$(tempfile)
    cat >$tmp <<EOF
>sp|O14683|P5I11_HUMAN Tumor protein p53-inducible protein 11
MIHNYMEHLERTKLHQLSGSDQLESTAHSRIRKERPISLGIFPLPAGDGLLTPDAQKGGET
PGSEQWKFQELSQPRSHTSLKVSNSPEPQKAVEQEDELSDVSQGGSKATTPASTANSDVAT
IPTDTPLKEENEGFVKVTDAPNKSEISKHIEVQVAQETRNVSTGSAENEEKSEVQAIIEST
PELDMDKDLSGYKGSSTPTKGIENKAFDRNTESLFEELSSAGSGLIGDVDEGADLLGMGRE
VENLILENTQLLETKNALNIVKNDLIAKVDELTCEKDVLQGELEAVKQAKLKLEEKNRELE
EELRKARAEAEDARQKAKDDDDSDIPTAQRKRFTRVEMARVLMERNQYKERLMELQEAVRW
TEMIRASRENPAMQEKKRSSIWQFFSRLFSSSSNTTKKPEPPVNLKYNAPTSHVTPSVX
>sp|P04637|P53_HUMAN Cellular
IQVVSRCRLRHTEVLPAEEENDSLGADGT
PQLX
>sp|P10144|GRAB_HUMAN Granzyme B OS=Homo sapiens OX=9606
MQPILLLLAFLLLPRADAGEIIGGHEAKPHSRPYMAYLMIWDQKSLKRCGGFLIRD
WGSSINVTLGAHNIKEQEPTQQFIPVKRPIPHPAYNPKNFSNDIMLLQLERKAKRT
SNKAQVKPGQTCSVAGWGQTAPLGKHSHTLQEVKMTVQEDRKCEX
>sp|P13674|P4HA1_HUMAN Prolyl
VECCPNCRGTGMQIRIHQIGPGMVQQIQS
DGQKITFHGEGDQEPGLEPGDIIIVLDQK
SHPGQIVKHGDIKCVLNEGMX
>sp|Q06416|P5F1B_HUMAN Putative POU domain, class 5, transc
IVVKGHSTCLSEGALSPDGTVLATASHDGYVKFWQIYIEGQDEPRCLHEWKP
HDGRPLSCLLFCDNHKKQDPDVPFWRFLITGADQNRELKMWCTVSWTCLQTI
RFSPDIFSSVSVPPSLKVCLDLSAEYLILSDVQRKVLYVMELLQNQEEGHAC
FSSISEFLLTHPVLSFGIQVVSRCRLRHTEVLPAEEENDSLGADGTHGAGAX
>sp|Q7Z4N8|P4HA3_HUMAN
MTEQMTLRGTLKGHNGWVTQIA
YGIPQRALRGHSHFVSDVVISS
GHTKDVLSVAFSSDNRQIVSGS
VRFSPNSSNPIIVSX
>sp|Q96A73|P33MX_HUMAN Putative
RNDDDDTSVCLGTRQCSWFAGCTNRTWNSSA
VPLIGLPNTQDYKWVDRNSGLTWSGNDTCLY
SCQNQTKGLLYQLFRNLFCSYGLTEAHGKWR
CADASITNDKGHDGHRTPTWWLTGSNLTLSV
NNSGLFFLCGNGVYKGFPPKWSGRCGLGYLV
PSLTRYLTLNASQITNLRSFIHKVTPHRX
>sp|Q9UHX1|PUF60_HUMAN Poly(U)-binding-splicing
MGKDYYQTLGLARGASDEEIKRAYRRQALRYHPDKNKEPGAEEKFKE
IAEAYDVLSDPRKREIFDRYGEEGLKGSGPSGGSGGGANGTSFSYTF
HGDPHAMFAEFFGGRNPFDTFFGQRNGEEGMDIDDPFSGFPMGMGGF
TNVNFGRSRSAQEPARKKQDPPVTHDLX
EOF
    echo "--regstart"
    echo start
    2search  -t '\|' -k2  --recstart '>' $tmp O14683
    echo middle
    2search  -t '\|' -k2  --recstart '>' --recend '' $tmp Q96A73
    echo end
    2search  -t '\|' -k2  --recstart '>' $tmp Q9UHX1

    echo "--regstart + --regend"
    echo start
    2search  -t '\|' -k2  --recstart '>' --recend '\n' $tmp O14683
    echo middle
    2search  -t '\|' -k2  --recstart '>' --recend '\n' $tmp Q96A73
    echo end
    2search  -t '\|' -k2  --recstart '>' --recend '\n' $tmp Q9UHX1

    rm $tmp
}

export -f $(compgen -A function | grep test_)
compgen -A function | grep test_ | sort | parallel -j6 --tag -k '{} 2>&1' > regressiontest.new
diff -Naur regressiontest.new regressiontest.out
