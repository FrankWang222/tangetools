
			       PLOTPIPE

		      - plot data from a pipe -


URL: https://gitlab.com/ole.tange/tangetools/-/tree/master/plotpipe

We have all been there: You have a bunch of data from a pipe that you
would like to get a better understanding of.

You know you can plot them by saving the data to a file, opening the
file in a spreadsheet, and making a graph; but it is just too much
bother because you do not need a fancy graph: You just need a quick
graph based on the data, and spending 5 minutes on generating that
graph is just too much hassle.

Plotpipe is designed for this situation.

Plotpipe reads data from a pipe (or a file) and plots it. If the input
is a CSV-file it tries to autodetect the separator and whether there
is a column header. It assumes the first column is the x-axis and that
all other columns are data series. If there is only a single
column, the line number is treated as the x-axis.

Examples:

    seq 1 100 | plotpipe
    seq 1 100 | shuf | plotpipe --smooth 10 --log y
    paste <(seq 1 100) <(seq 1 100) <(seq 1 100 | shuf) | plotpipe
    (echo "#Title"; echo "#Subtitle";
     printf "Column 1\tColumn 2\tColumn 3\n";
     paste <(seq 1 100) <(seq 51 150) <(seq 1 100 | shuf) ) | plotpipe

Copyright (C) 2020-2021 Ole Tange, http://ole.tange.dk and Free
Software Foundation, Inc.

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
GNU plotpipe comes with no warranty.
